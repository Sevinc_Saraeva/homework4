
import java.util.Arrays;
import java.util.Random;


public class Human {
    String name;
    String surname;
    int year;
    int iq_level;
    Pet pet;
    Human mother;
    Human father;
    String[][] Shedule;

    public Human(String name, String surname, int year) {
        this.name = name;
        this.surname = surname;
        this.year = year;
    }

    public Human() {
    }

    public Human(String name, String surname, int year, Human mother, Human father) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.mother = mother;
        this.father = father;
    }

    public Human(String name, String surname, int year, int iq_level, Pet pet, Human mother, Human father, String[][] shedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq_level = iq_level;
        this.pet = pet;
        this.mother = mother;
        this.father = father;
        this.Shedule = shedule;
    }

    public void greetPet(){
        System.out.println("Hello, " + this.pet.nickname);

    }
    public void describePet(){
        System.out.print("I have " + this.pet.species + ", he is " + this.pet.age + " years old. ");
        if(pet.trickLevel<50) System.out.println("He is almost not sly");
        else System.out.println("He is sly");
    }

    @Override
    public String toString() {
        return "Human{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", year=" + year +
                ", iq_level=" + iq_level +
                ", mother=" + mother.name + " " + mother.surname+
                ", father=" + father.name + " " + father.surname+
                ", pet=" + pet.toString() +
                '}';
    }
    public boolean feedPet(boolean timefeed) {
        if (timefeed) {
            feedPetAndMessage(timefeed);
            return true;
        }
        Random random = new Random();
        int rand = random.nextInt(100 - 1 + 1) + 1;
        if (rand > this.pet.trickLevel) {
            feedPetAndMessage(true);
            return true;
        } else {
            System.out.println("I think Jack is not hungry.");
            return false;
        }
    }
      public boolean feedPetAndMessage(boolean b){
          System.out.println("Hm... I will feed Jack's " + pet.nickname);
          return true;
      }
}
