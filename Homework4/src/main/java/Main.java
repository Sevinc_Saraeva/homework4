public class Main {
    public static void main(String[] args) {
        Pet dog = new Pet("dog", "Rick");
        Pet cat = new Pet("cat", "aa", 2,23, new String []{"walk", "jump", "sleep"});
        Pet cat2 = new Pet("dog", "dogname",1,45, new String[]{"walk"});
        Pet horse = new Pet();
        horse.species = "horse";
        horse.nickname= "Jack";
        horse.age = 6;
        horse.trickLevel = 45;
        horse.habits = new String[] {"run", "eat"};

        Human Michael = new Human("Michael", "AAA", 1970);
        Michael.iq_level = 70;
        Michael.pet = horse;
        Michael.mother = new Human("DDD", "BBB", 1940);
        Michael.father = new Human("EEE", "BBB", 1940);
        Human Laura = new Human("Laura", "AAA", 1970);
        Laura.mother = new Human("RRR", "TTT", 1940);
        Laura.father = new Human("YYY", "TTT", 1940);
        Laura.pet = new Pet("Hamster", "Ham",2,23, new String []{"walk", "jump", "sleep"});
        Laura.iq_level = 90;
        Human Alex = new Human("Alex", "AAA", 2000, Michael, Laura);
        Alex.pet = cat;

        Human Beck = new Human("Beck", "CCC", 1980);
        Beck.mother = new Human("DDD", "BBB", 1940);
        Beck.father = new Human("EEE", "BBB", 1940);
        Beck.pet = cat2;
        Human Joe = new Human();
        Joe.name = "Joe";
        Joe.surname = "CCC";
        Joe.mother = new Human("RRR", "TTT", 1940);
        Joe.father = new Human("YYY", "TTT", 1940);
        Joe.year = 1980;
        Joe.iq_level = 90;
        Joe.pet = dog;

        Human Lala = new Human("Lala", "CCC", 2003,59,cat2, Beck, Joe, new String[][]{{"Monday"}, {"go to school"}});

        cat.eat();
        cat.foul();
        cat.respond();
        Alex.greetPet();
        Alex.describePet();
        System.out.println(Michael.toString());
        System.out.println(Laura.toString());
        System.out.println(Lala.toString());
        System.out.println(Beck.toString());
        System.out.println(Joe.toString());
        Michael.feedPet(true);
        //Michael.feedPet(false);


    }
}
